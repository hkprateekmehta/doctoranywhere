  var server = "http://3.14.13.21:8081";
  //var server = "http://localhost:8081";

$(document).ready(function(){
  //initialize the firebase app

  var config = {
    apiKey: "AIzaSyCJxdFx3CzoExnQRpVcWnjSQ5VcRhjhfCU",
    authDomain: "doctor-4bac1.firebaseapp.com",
    databaseURL: "https://doctor-4bac1.firebaseio.com",
    projectId: "doctor-4bac1",
    storageBucket: "doctor-4bac1.appspot.com",
    messagingSenderId: "562864823",
    appId: "1:562864823:web:8333d69713c930afe1e332"
  };
  firebase.initializeApp(config);

  //create firebase references
  var Auth = firebase.auth(); 
  var dbRef = firebase.database();
  var contactsRef = dbRef.ref('contacts')
  var usersRef = dbRef.ref('users')
  var auth = null;

  //Register
  $('#registerForm').on('submit', function (e) {
    e.preventDefault();
    $('#registerModal').modal('hide');
    $('#messageModalLabel').html(spanText('<i class="fa fa-cog fa-spin"></i>', ['center', 'info']));
    $('#messageModal').modal('show');
    var data = {
      email: $('#registerEmail').val(), //get the email from Form
      firstName: $('#registerFirstName').val(), // get firstName
      lastName: $('#registerLastName').val(), // get lastName
    };
    var passwords = {
      password : $('#registerPassword').val(), //get the pass from Form
      cPassword : $('#registerConfirmPassword').val(), //get the confirmPass from Form
    }
    if( data.email != '' && passwords.password != ''  && passwords.cPassword != '' ){
      if( passwords.password == passwords.cPassword ){
        //create the user
        
        firebase.auth()
          .createUserWithEmailAndPassword(data.email, passwords.password)
          .then(function(user) {
            return user.updateProfile({
              displayName: data.firstName + ' ' + data.lastName
            })
          })
          .then(function(user){
            //now user is needed to be logged in to save data
            auth = user;
            //now saving the profile data
            usersRef.child(user.uid).set(data)
              .then(function(){
                console.log("User Information Saved:", user.uid);
              })
            $('#messageModalLabel').html(spanText('Success!', ['center', 'success']))
            
            $('#messageModal').modal('hide');
          })
          .catch(function(error){
            console.log("Error creating user:", error);
            $('#messageModalLabel').html(spanText('ERROR: '+error.code, ['danger']))
          });
      } else {
        //password and confirm password didn't match
        $('#messageModalLabel').html(spanText("ERROR: Passwords didn't match", ['danger']))
      }
    }  
  });

  //Login
  $('#loginForm').on('submit', function (e) {
    e.preventDefault();
    $('#loginModal').modal('hide');
    $('#messageModalLabel').html(spanText('<i class="fa fa-cog fa-spin"></i>', ['center', 'info']));
    $('#messageModal').modal('show');

    if( $('#loginEmail').val() != '' && $('#loginPassword').val() != '' ){
      //login the user
      var data = {
        email: $('#loginEmail').val(),
        password: $('#loginPassword').val()
      };
      firebase.auth().signInWithEmailAndPassword(data.email, data.password)
        .then(function(authData) {
          auth = authData;
          $('#messageModalLabel').html(spanText('Success!', ['center', 'success']))
          $('#messageModal').modal('hide');
        })
        .catch(function(error) {
          console.log("Login Failed!", error);
          $('#messageModalLabel').html(spanText('ERROR: '+error.code, ['danger']))
        });
    }
  });

  $('#logout').on('click', function(e) {
    e.preventDefault();
    firebase.auth().signOut();
    $('div#table').addClass('hidden');
  });

  //save contact
  $('#contactForm').on('submit', function( event ) {  
    event.preventDefault();
    if( auth != null ){
      if( $('#name').val() != '' || $('#email').val() != '' ){
        contactsRef.child(auth.uid)
          .push({
            name: $('#name').val(),
            email: $('#email').val(),
            location: {
              city: $('#city').val(),
              state: $('#state').val(),
              zip: $('#zip').val()
            }
          })
          document.contactForm.reset();
      } else {
        alert('Please fill at-lease name or email!');
      }
    } else {
      //inform user to login
    }
  });

  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      auth = user;
      var token;
      console.log("----------------------------------------------------");
      user.getIdToken().then(function(idToken){
          console.log(idToken);
          $('#token').val(idToken);
          token = idToken;
          console.log("----------------------------------------------------");
      });


var handle = setInterval( function(){

  if(typeof token !=="undefined"){
	  
   const urlParams = new URLSearchParams(window.location.search);

   $.ajax({
          url : server+'/api/users?'+urlParams,
          type : 'GET',
          dataType:'json',
          headers: { 'X-Firebase-Auth': $('#token').val() },
          success : function(data, textStatus, response) {              
		  
			  var totalpages=response.getResponseHeader("x-page-count");
			  var linkHTML='';
			  const myParam = urlParams.get('page');
			  $('div#pagination').html("");
			  
			  console.log(myParam);
			  if(totalpages>1){
				  $('div#pagination').removeClass('hidden');
			  for (i = 0; i < totalpages; i++) {
				  if(myParam==(i) || (!myParam && i==0)){
					  linkHTML += '<a class="active" href='+window.location.href.split('?')[0]+'?page='+i+ '>'+(i+1)+'</a>';
				  }else{
					  linkHTML += '<a  href='+window.location.href.split('?')[0]+'?page='+i+ '>'+(i+1)+'</a>';
				  }
				}
				$('div#pagination').append(linkHTML);
			  }

              if(data!=null && data.length>0){
                $('div#table').removeClass('hidden');
              }

              $.each(data,function(index,value){
				  var html='';
				  if(value.isActive==false){
					  
					  html = '<div class="Row disabled"><div class="Cell"><p>'+value.firstName+'</p></div><div class="Cell"><p>'+value.lastName+'</p></div><div class="Cell"><p>'+value.contactNo+'</p></div><div class="Cell"><p>'+value.addressList[0].line1+'</p></div><div class="Cell"><p>'+value.addressList[0].line2+'</p></div><div class="Cell"><p>'+value.addressList[0].city+'</p></div><div class="Cell"><p>'+value.addressList[0].country+'</p></div><div class="Cell"><p>'+value.addressList[0].postalcode+'</p></div><div class="Cell"><button type="button" class="btn btn-primary addValue"  data-dismiss="modal" onclick="deletePatient('+value.id+')">Delete</button></br></div></div>';
					  
					  
				  }else{
						html = '<div class="Row"><div class="Cell"><p>'+value.firstName+'</p></div><div class="Cell"><p>'+value.lastName+'</p></div><div class="Cell"><p>'+value.contactNo+'</p></div><div class="Cell"><p>'+value.addressList[0].line1+'</p></div><div class="Cell"><p>'+value.addressList[0].line2+'</p></div><div class="Cell"><p>'+value.addressList[0].city+'</p></div><div class="Cell"><p>'+value.addressList[0].country+'</p></div><div class="Cell"><p>'+value.addressList[0].postalcode+'</p></div><div class="Cell"><button type="button" class="btn btn-primary addValue"  data-dismiss="modal" onclick="deletePatient('+value.id+')">Delete</button></br><button type="button" class="btn btn-success"  data-dismiss="modal" onclick="softDeletePatient('+value.id+')">Soft Delete</button></div></div>';
				  }				  
                  $('div#table').append(html);
              });
			  
			  
              
          },
          error : function(request,error)
          {
              alert("Request: "+JSON.stringify(request));
          }
      });

      clearInterval(handle);

  }

}, 10);

      $('body').removeClass('auth-false').addClass('auth-true');
      usersRef.child(user.uid).once('value').then(function (data) {
        var info = data.val();
        if(user.photoUrl) {
          $('.user-info img').show();
          $('.user-info img').attr('src', user.photoUrl);
          $('.user-info .user-name').hide();
        } else if(user.displayName) {
          $('.user-info img').hide();
          $('.user-info').append('<span class="user-name">'+user.displayName+'</span>');
        } else if(info.firstName) {
          $('.user-info img').hide();
          $('.user-info').append('<span class="user-name">'+info.firstName+'</span>');
        }
      });
      contactsRef.child(user.uid).on('child_added', onChildAdd);
    } else {
      // No user is signed in.
      $('body').removeClass('auth-true').addClass('auth-false');
      auth && contactsRef.child(auth.uid).off('child_added', onChildAdd);
      $('#contacts').html('');
      auth = null;
    }
  });
});

function onChildAdd (snap) {
  $('#contacts').append(contactHtmlFromObject(snap.key, snap.val()));
}
 
//prepare contact object's HTML
function contactHtmlFromObject(key, contact){
  return '<div class="card contact" style="width: 18rem;" id="'+key+'">'
    + '<div class="card-body">'
      + '<h5 class="card-title">'+contact.name+'</h5>'
      + '<h6 class="card-subtitle mb-2 text-muted">'+contact.email+'</h6>'
      + '<p class="card-text" title="' + contact.location.zip+'">'
        + contact.location.city + ', '
        + contact.location.state
      + '</p>'
      // + '<a href="#" class="card-link">Card link</a>'
      // + '<a href="#" class="card-link">Another link</a>'
    + '</div>'
  + '</div>';
}

function spanText(textStr, textClasses) {
  var classNames = textClasses.map(c => 'text-'+c).join(' ');
  return '<span class="'+classNames+'">'+ textStr + '</span>';
}

function addPatient(){
var firstName = $('#firstname').val();
var lastName =  $('#lastname').val();
var contactnumber = $('#contactnumber').val();
var address = [];
address.push({
  'line1':$('#line1').val(),
  'line2':$('#line2').val(),
  'city': $('#city').val(),
  'country':$('#country').val(),
  'postalcode': $('#zip').val()
});

var requestData={
  'firstName': firstName,
  'lastName': lastName,
  'contactNo': contactnumber,
  'addressList': address
}



$.ajax({

          url : server+'/api/users',
          type : 'POST',
          data : JSON.stringify(requestData),
          dataType:'json',
          contentType: "application/json; charset=utf-8",
          headers: { 'X-Firebase-Auth': $('#token').val() },
          success : function(data) {              
              location.reload(true);
          },
          error : function(request,error)
          {
              alert("Request: "+JSON.stringify(request));
          }
      })

}

function deletePatient(id){
$.ajax({

          url : server+'/api/users/'+id,
          type : 'DELETE',
          headers: { 'X-Firebase-Auth': $('#token').val() },
          success : function(data) {              
		  		  location.reload(true);
          },
          error : function(request,error)
          {
              alert("Request: "+JSON.stringify(request));
          }
      })

}
function softDeletePatient(id){
$.ajax({

          url : server+'/api/users/inactive/'+id,
          type : 'GET',
          headers: { 'X-Firebase-Auth': $('#token').val() },
          success : function(data) {              
		  		  location.reload(true);
          },
          error : function(request,error)
          {
              alert("Request: "+JSON.stringify(request));
          }
      })

}

